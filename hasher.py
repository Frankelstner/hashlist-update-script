from ctypes import *

try:
    lib = cdll.LoadLibrary("payday2hash32")
except:
    lib = cdll.LoadLibrary("payday2hash64")

def hasher(text, hashlib = lib):
    num = c_ulonglong(0)
    hashlib.Hash64(byref(num), text, c_ulonglong(len(text)), c_ulonglong(0))
    return num.value
