import os, sys,re
from struct import pack, unpack
from binascii import hexlify, unhexlify
from ctypes import *
from hasher import hasher

p = r"C:\Program Files (x86)\Steam\steamapps\common\PAYDAY 2\assets"


def HashList(res = {}):
    """Create dict hash vs name."""
    if res: return res
    res.update({hasher(line[:-2]):line[:-2] for line in open("hashlistFULL","rb")})
    return res

def ReadDb():
    """Read bundle_db.blb file. Return dict index vs (nameHash, typeHash)."""
    # bundle_db.blb:
    #   Little endian.
    #   Number of entries appears several times: 27619
    #
    #   ints
    #   a b c d
    #   e f g h
    #   i j k l
    #   m n
    #
    #   a = 5C9CF100
    #   b = c = 17  = structs with 10 bytes count
    #   d = 38 = offset to structs with 10 bytes
    #   e = 4CF01800
    #   f = 010F183D
    #   g = 5C9CF100
    #   h = i = 27619 = structs with 20 bytes count
    #   j = 1a8 = offset to structs with 20 bytes
    #   k = 4CF01800
    #   l = 012A0A7A
    #   m = 27619 = h=i
    #   n = 0
    #
    #   @38
    #   10 byte structs:
    #       8 hash
    #       4 bitflag (one bit only spread over 32 places)
    #       4 size?
    #
    #   @1a8
    #   20 byte structs:
    #       
    #       8: hash of type
    #       8: hash of file path
    #       4: bitflag (at most 1 bit set)
    #       4: int
    #       4: index
    #       4: int
    #
    #   D9A7BEA0

    f = open(p+"/bundle_db.blb","rb")
    version =  hexlify(f.read(4)) # == "5c9cf100"
    structCount, structCount2, structOffset = unpack("III",f.read(12))
    v1 = f.read(4)
    #assert hexlify(f.read(4)) == "4cf01800" #"010f183d" #"5c9cf100"
    f.read(4)
    assert hexlify(f.read(4)) == version
    entryCount, entryCount2, entryOffset = unpack("III",f.read(12))
    assert f.read(4) == v1 # "4cf01800" #"012a0a7a"
    f.read(4)
    entryCount3, null = unpack("II",f.read(8))

    assert structCount==structCount2 and entryCount==entryCount2#==entryCount3


    tmp = []
    for i in xrange(structCount):
        somehash, oneBit, size = unpack("QII", f.read(16))
        tmp.append((somehash, somehash))


    rv = {}
    
    for i in xrange(entryCount):
        typeHash, nameHash, oneBit, n1, index, n2 = unpack("QQIIII",f.read(32))
##        if not index in rv:
##        print oneBit, n1, index, n2

        # oneBit defines localization.
##        if index not in rv or oneBit==0:
        rv[index] = (nameHash, typeHash)

##        if 1687 in rv:
##            print rv[1687]

        # 0: Dutch
##32 Dutch
##64 Dutch
##128 1690
##256 1691
##131072 1692
##262144 1693

##            if nameHash==10147212032884564710:
##                print oneBit, index

    for i in xrange(index,0x7fffffff):
        if i not in rv:
            rv[i] = tmp.pop()
        if not tmp: break

    f.close()
    return rv


hashlist = HashList()
db = ReadDb()

##for n,typ in db.itervalues():
####    print typ
####    print hashlist[typ]
##    if typ not in hashlist:
####        print typ
##        if n in hashlist:
##            print hashlist[n]


##abc = "abcdefghijklmnopqrstuvwxyz"
##abc+=abc.upper()
##abc+="0123456789_."
##abc=[c for c in abc]
##abc.append("")
##
##for a in abc:
##    for b in abc:
##        for c in abc:
##            assert hasher("lua"+a+b+c)!=9130335355039793855
##
##asf
##
##
##LuaJIT 2.1.0-beta3

# 9130335355039793855
##sdf


## Readers for _h files, return dict index vs (offset, size).

hexaRegex = re.compile("[a-f0-9]{16}")

def AllMeta(source):
    """Return the meta of an all bundle. index vs (offset, size)"""

    # "all":
    #     Section size (including this)
    #     F297A000    (presumably type)
    #     File count
    #     File count
    #     18000000
    #     ACEC1800
    #     01ED1800
    #     For i in file count:
    #         index
    #         offset
    #         size
    #           
    #         0 size => 0 offset
    #
    #     *new section*
    #     191FC594
    #     00000000

    f = open(source, "rb")

    fileSize = os.fstat(f.fileno()).st_size
    metaSize, version, fileCount, fileCount2, hasPayload, const = unpack("I4sIII8s",f.read(28))
    assert metaSize == fileSize - 8
##    assert hexlify(version) == "f297a000"
    assert hasPayload==24
    assert fileCount==fileCount2
##    assert hexlify(const) == "acec180001ed1800"

    rv = {}
    for i in xrange(fileCount):
        index,offset,size = unpack("III",f.read(12))

        rv[index]= (offset, size)
        

    assert hexlify(f.read(8)) == "191fc59400000000"
    assert f.tell() == fileSize
    f.close()

    return rv



def AllMeta(source):
    f = open(source,"rb")
    eof, bundleCount, _,_,_ = unpack("5I",f.read(20))
    offsets, entryCounts = [], []
    for i in xrange(bundleCount):
        index, entryCount, entryCount2, offset, one = unpack("QIIQI",f.read(28))
        assert entryCount == entryCount2
        assert index == i
        assert one == 1

        entryCounts.append(entryCount)
        offsets.append(offset+4)

    rv = []

    for i in xrange(bundleCount):
        dat = {} #OrderedDict()
        f.seek(offsets[i])
        for entry in xrange(entryCounts[i]):
            index, offset, size = unpack("III",f.read(12))
            dat[index] = (offset, size)
        rv.append(dat)
    f.close()
    return rv

##if os.path.exists(p+"/all_h.bundle"):
##    target = os.path.dirname(__file__)+"/all_h.bundle"
##    if os.path.exists(target):
##        os.remove(target)
##    os.rename(p+"/all_h.bundle", target)
##
##for i, dat in enumerate(AllMeta("all_h.bundle")):
##    f = open(p+"/all_"+str(i)+"_h.bundle","wb")
##    fileSize = 7*4 + len(dat)*12
##    f.write(pack("I",fileSize))
##    f.write(unhexlify("F297A000"))
##    f.write(pack("II",len(dat),len(dat)))
##    f.write(unhexlify("18000000ACEC180001ED1800"))
##    for index, (offset, size) in dat.iteritems():
##        f.write(pack("III", index, offset, size))
##    f.write(unhexlify("191FC59400000000"))
##    f.close()
##    
##asdf




def HexaMeta(source):
    """Read the meta of a hexadecimal bundle (non-all)."""
    #   _h.bundle:
    #       0: Section size (including this)
    #       4: File count
    #       8: File count
    #       c: 10000000 (bundle has payload) or 00000000 (bundle has 0 bytes)
    #       10: ACEB1800
    #       14: For i in file count:
    #           index
    #           offset
    #
    #
    #       F8C5FCEB
    #       Section size (including this)
    #       16-hash count
    #       set([1270, 310, 70, 2550, 10, 630, 150, 30]) (always >= 16-hash count)
    #       10000000
    #       28CDEE00
    #       16-hashes
    #
    #
    #       1F46B719
    #       8-hash count
    #       8-hashes
    f = open(source, "rb")
    fileSize = os.fstat(f.fileno()).st_size

    metaSize, fileCount, fileCount2, hasPayload = unpack("4I",f.read(16))

    if not hasPayload:
        f.close()
        return {}

    assert fileCount==fileCount2
    v1 = f.read(4)


    # Calculate size from offsets.

    payloadSize = os.stat(source[:-9]+".bundle").st_size
    
    indexes = []
    offsets = []
    
    for i in xrange(fileCount):
        index, offset = unpack("II", f.read(8))
        indexes.append(index)
        offsets.append(offset)
    offsets.append(payloadSize)

##    if "f8b759c0f7048df4" in source:
##        for i,o in zip(indexes, offsets):
##            print i,o
##        asdf
    
    rv = {}

    for i in xrange(fileCount):
        rv[indexes[i]] = (offsets[i], offsets[i+1]-offsets[i])

    # Not sure what to do with the rest of the file. Just make sure it is valid.
    assert f.tell()== metaSize
    assert hexlify(f.read(4))=="f8c5fceb"


    midStart = f.tell()
    metaSize, hash16Count, c, always16 = unpack("IIII",f.read(16))
    assert always16 == 16
    version =  hexlify(f.read(4)) #=="28cdee00"
    
    for i in xrange(hash16Count):
        hashed = f.read(16)

    assert f.tell()-midStart == metaSize
    assert hexlify(f.read(4))=="1f46b719"

    hash8Count = unpack("I", f.read(4))[0]
    for i in xrange(hash8Count):
        hashed = f.read(8)
    
    assert f.tell()==fileSize
    
    f.close()

    return rv


##def Meta(fname):
##    """For a given bundle, return index vs (offset, size) for all payloads."""
##    path = p+"/"+fname
##    eof = os.stat(path).st_size
##    if not eof: return {}
##    return HexaMeta(path) if hexaRegex.match(fname) else AllMeta(path)



def FullMeta(meta = {}):
    """Make a dict with all meta information to retrieve the payload from any file:

    For each entry, store: (nameHash, typeHash) vs (bundle name, offset, size)"""
    
    if meta: return meta

    db = ReadDb()

    for fname in os.listdir(p):
        path = p+"/"+fname
        eof = os.stat(path).st_size
        if not eof: continue

        if fname == "all_h.bundle":
            metas = AllMeta(path)
            for i, fmeta in enumerate(metas):
                for index, (offset, size) in fmeta.iteritems():
                    meta[db[index]] = ("all_"+str(i),offset,size)
            
            
        elif "all" not in fname and fname.endswith("_h.bundle"):
            fmeta = HexaMeta(path)

            for index, (offset, size) in fmeta.iteritems():
                if db[index] not in meta:  # Hopefully guarantee English localization.
                    meta[db[index]] = (fname[:-9], offset, size)
            

    return meta


def HalfMeta(meta = {}):
    """Make a dict where only name hash is stored. It may return a file of any type; make sure it is unique."""
    if meta: return meta
    meta.update({key[0]:val for key, val in FullMeta().iteritems()})
    return meta





# Archives:
#   all_i.bundle
#   all_i_h.bundle    DEPRECATED, use all_h.bundle instead.
#   *16 hex chars*.bundle
#   *16 hex chars*_h.bundle
#
#   _h.bundle are meta for .bundle (which contain payload only).


# Things I want:
#   Just dump all:
#       For each bundle, for each file, grab payload and lookup index in main file.
#
#   Extract certain files:
#       Gather all meta from all bundles and main.
#
#       For each wanted file (given by hash or fname), retrieve payload and return it.
#



# Tools:
#   HashList(): Create dict hash vs name
#   ReadDb(): Create dict index vs (nameHash, typeHash)
#   Meta(fname): Create dict index vs (offset, size)


# 





def open2(path):
    folderPath = os.path.dirname(path)
    if not os.path.isdir(folderPath): os.makedirs(folderPath)
    return open(path,"wb")


import hashlib


def Dump(target, bundleName=""):
    """Extract all files to target folder. Extract all bundles if no bundleName is given, else extract only that one. 

    For each bundle:
        For each file index+payload:
            Retrieve the hash from the index.
            Retrieve the path from the hash.
            Extract the payload into path.
    """

    db = ReadDb()
    hashlist = HashList()

##    md5s = set()
##    doLater = []

    target = target.replace("\\","/")
    if target[-1]!="/": target+="/"
    
    for (pathHash, typeHash), (bundleName, offset, size) in FullMeta().iteritems():

        f = open(p+"/"+bundleName+".bundle","rb")
        typ = hashlist.get(typeHash, str(typeHash))
        try: path = hashlist[pathHash]
        except KeyError: path = hexlify(pack("Q",pathHash))
        if typ=="texture": typ = "dds"
        targetPath = target + path + "." + typ

        if os.path.exists(targetPath):# or "pd2_dlc_old" not in targetPath:
            continue

        
        f.seek(offset)
        data = f.read(size)
        if typeHash==LUA: data = Decrypt(data)
        f2 = open2(targetPath)
        f2.write(data)
        f2.close()
    

    
##    for fname in os.listdir(p):
##        if bundleName and not fname.startswith(bundleName): continue
##        if fname.endswith("_h.bundle"):
##            meta = Meta(fname)
##
##            f = open(p+"/"+fname[:-9]+".bundle","rb")
##
##            for index, (offset, size) in meta.iteritems():
##                pathHash, typeHash = db[index]
##
##                typ = hashlist[typeHash]
##                try: path = hashlist[pathHash]
##                except KeyError: path = hexlify(pack("Q",pathHash))
##
##
##                if typ=="texture": typ = "dds"
##                targetPath = target + path + "." + typ
##
##                
##                f.seek(offset)
##                data = f.read(size)
####                m = hashlib.md5(data).digest()
##
####                if pathHash not in hashlist:
####                    doLater.append((target, path, typ, data, m))
####                    continue
##                
####                if m in md5s and pathHash not in hashlist:
####                    targetPath = target + "DUPLICATES/"+hexlify(pack("Q",pathHash))+"."+typ
####                md5s.add(m)
##                
##                if os.path.exists(targetPath):
####                    print "-"+targetPath
##                    continue  # Ignore existing files (the same file may be found in several bundles).
####                print "+"+targetPath
####                if m in md5s and pathHash not in hashlist:
####                    targetPath = target + "DUPLICATES/"+hexlify(pack("Q",pathHash))+"."+typ
####                    
####                md5s.add(m)
##                f2 = open2(targetPath)
##                f2.write(data)
##                f2.close()
##            f.close()

##    for target, path, typ, data, m in doLater:
##        if m in md5s:
##            targetPath = target + "DUPLICATES/"+path+"."+typ
##        else:
##            targetPath = target + path +"."+typ
##        f2 = open2(targetPath)
##        f2.write(data)
##        f2.close()



def ExtractPath(path): return ExtractHash(hasher(path))
def ExtractHash(nameHash):
    """Yield the payload corresponding to the hash."""
    fname, offset, size = HalfMeta()[nameHash]
    f = open(p+"/"+fname+".bundle","rb")
    f.seek(offset)
    data = f.read(size)
    f.close()
    return data

def ExtractHashType(nameHash, typ):
    fname, offset, size = FullMeta()[(nameHash, hasher(typ))]
    f = open(p+"/"+fname+".bundle","rb")
    f.seek(offset)
    data = f.read(size)
    f.close()
    return data


def ExtractHashes(nameHashes):
    """Yield the payloads corresponding to the hashes."""
    meta = HalfMeta()
                                     
    
    for nameHash in nameHashes:
        if nameHash not in meta: continue # Invalid file given. Just keep going.
        
        fname, offset, size = meta[nameHash]
        f = open(p+"/"+fname+".bundle","rb")
        f.seek(offset)
        data = f.read(size)
        f.close()
        yield nameHash, data

def ExtractPathTypes(paths, constType=""):
    for data in ExtractHashTypes((hasher(path) for path in paths), constType):
        yield data
def ExtractHashTypes(hashes,constType=""):
    meta = FullMeta()

    if constType:
        typ = hasher(constType)
        for nameHash in hashes:
            if (nameHash,typ) not in meta: continue # Invalid file given. Just keep going.
            
            fname, offset, size = meta[(nameHash,typ)]
            f = open(p+"/"+fname+".bundle","rb")
            f.seek(offset)
            data = f.read(size)
            f.close()
            yield data
    else:
        for nameHash, typ in hashes:
            if nameHash not in meta: continue # Invalid file given. Just keep going.
            
            fname, offset, size = meta[(nameHash, hasher(typ))]
            f = open(p+"/"+fname+".bundle","rb")
            f.seek(offset)
            data = f.read(size)
            f.close()
            yield data
    
                                    
def ExtractType(typ=None):
    """Yield all payloads once."""
    db = ReadDb()

    wantedHash = hasher(typ) if typ!=None else None

    hasBeenYielded = set()  # If a file exist in several bundles, yield only once.
    
    for (pathHash, typeHash), (bundleName, offset, size) in FullMeta().iteritems():
        if wantedHash is None or typeHash==wantedHash:
            if (pathHash, typeHash) not in hasBeenYielded:
                hasBeenYielded.add((pathHash, typeHash))
                f = open(p+"/"+bundleName+".bundle","rb")
                f.seek(offset)
                data= f.read(size)
                if typeHash==LUA: data = Decrypt(data)
                yield data

                    
def Decrypt(data):
    key = [ord(c) for c in "asljfowk4_5u2333crj"]
    n = len(key)
    size = len(data)
    return "".join(chr(  ord(c) ^ ((key[(((size+i)*7)%n)] * (size-i))%256)  )  for i,c in enumerate(data)) 


LUA = 9130335355039793855 # hasher("lua51")

if __name__ == "__main__":
##    rv =  ExtractPath("strings/blackmarket")
##    f = open("D:/mydump/strings/blackmarket.strings","wb")
##    f.write(rv)
##    f.close()
##    for line in rv.split():
##        print line
    Dump(r"D:/mydump")
##    Dump(r"D:/mydump2", "31c106044eeaf778_h")
    
    
