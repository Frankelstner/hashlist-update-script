Source scripts to update the hashlist. 

hashlistFULL is the actual list used in processing.  
All new strings are added to it and never removed.

hashlist is produced at the end by removing all paths and language ids from hashlistFULL that have  
no corresponding hash in the current game bundles.

teststrings.py is the main script with many different methods to retrieve a good pool of strings to test.  
It relies on all the other scripts. It requires some path adjustments at the top.

extract.py extracts the game files into D:/mydump when executed, using the hashlist.  
It also has some functions to grab payloads.  
It requires path adjustment at the top to be correctly used by teststrings.py.

hasher.py + payday2hash64.dll is utility to create a hash out of a string.

converter.py is an extremely stripped-down version of my mission converter with the sole  
purpose of helping me walk along depedencies of mission scripts.
