# -*- coding: utf-8 -*-
import os
from binascii import hexlify, unhexlify
from struct import pack, unpack
from collections import OrderedDict
from StringIO import StringIO

try:
    #try to print a number as 0.95
    from ctypes import *
    floatlib = cdll.LoadLibrary("floattostring")
    def formatfloat(num):
        bufType = c_char * 100
        buf = bufType()
        bufpointer = pointer(buf)
        floatlib.convertNum(c_double(num), bufpointer, 100)
        rawstring=(buf.raw)[:buf.raw.find("\x00")]
        if rawstring[:2]=="-.": return "-0."+rawstring[2:]
        elif rawstring[0]==".": return "0."+rawstring[1:]
        elif "e" not in rawstring and "." not in rawstring: return rawstring+".0"
        return rawstring
except:
    #the number will be printed as 0.949999988079
    def formatfloat(num):
        return str(num)
    

class BlockMeta:
    def __init__(self, f):
        magic, count, count2, offset = unpack("IIII",f.read(16))
        if magic != fileMagic or count != count2:  asdf

        self.count = count
        self.offset = offset

class AssignBlock:
    def __init__(self, f):
        index, count, count2, offset, magic = unpack("iIIII",f.read(20))
        if magic != fileMagic or count != count2: asdf

        self.index = index
        self.count = count
        self.offset = offset


class FieldMeta:
    def __init__(self, f):
        self.v1 = unpack("I",f.read(3)+"\0")[0]
        self.type1 = unpack("B",f.read(1))[0]
        self.v2 = unpack("I",f.read(3)+"\0")[0]
        self.type2 = unpack("B",f.read(1))[0]

class Stub: pass

def readFile(f):
    global fileMagic
    fileMagic = unpack("I",f.read(4))[0]
    f.seek(0)
    floatMeta = BlockMeta(f)
    keywordMeta = BlockMeta(f)
    vector3Meta = BlockMeta(f)
    vector4Meta = BlockMeta(f)
    stringMeta = BlockMeta(f)
    assignmentMeta = BlockMeta(f)

    headerTail = f.read(8)
    if hexlify(headerTail) != hexlify(pack("I",fileMagic))+"00000008": asdf

    f.seek(floatMeta.offset)
    floats = unpack(str(floatMeta.count)+"f",f.read(4*floatMeta.count))

    f.seek(keywordMeta.offset)
    keywordOffsets = [ unpack("II",f.read(8))  for i in xrange(keywordMeta.count)]
    for num in keywordOffsets: error if num[0] != fileMagic else 1
    keywordOffsets = [ offset[1]  for offset in keywordOffsets]
    keywords = []
    for offset in keywordOffsets:
        f.seek(offset)
        chars = ""
        while 1:
            char = f.read(1)
            if char=="\0": break
            if not char: asdfasdf
            chars+=char
        keywords.append(chars)

    f.seek(vector3Meta.offset)
    vector3s = [unpack("3f",f.read(12)) for i in xrange(vector3Meta.count)]
    
    f.seek(vector4Meta.offset)
    vector4s = [unpack("4f",f.read(16)) for i in xrange(vector4Meta.count)]
    
    f.seek(stringMeta.offset)
    strings = [hexlify(f.read(8)) for i in xrange(stringMeta.count)]
    
    f.seek(assignmentMeta.offset)
    assigns = [AssignBlock(f) for i in xrange(assignmentMeta.count)]
    for assign in assigns:
        if f.tell() != assign.offset and assign.count!=0: adf
        if assign.count != 0 and assign.offset == 0: asdf
        if assign.count == 0 and assign.offset != 0: asdf    

        assign.fieldMetas = []
        for fieldMeta in xrange(assign.count):
            assign.fieldMetas.append(FieldMeta(f))

    if hexlify(f.read()) != "70cd756d": ad
    
    f.close()

    return [None, False, True, floats, keywords, vector3s, vector4s, strings, assigns]

###############
###reading done
###############

#subclass strings so I can mostly deal with strings, but fall back to the raw data by accessing the "raw" attribute
class str2(str): pass
class namedDict(OrderedDict): pass


#make a hierarchy out of the separate lists
def createHierarchy(fData, assignIndex):
    #put it all into lists or dicts

    #world files do not use names anywhere, but sequence files do
    meta = fData[8][assignIndex]
    if meta.index==-1: name = None 
    else:
        name = fData[4][meta.index]

        
    fields = namedDict() 
    for fieldMeta in meta.fieldMetas:
        if fieldMeta.type2==1: #false
            val = str2("False")
            val.raw = False
        elif fieldMeta.type2==2: #true
            val = str2("True")
            val.raw = True
        elif fieldMeta.type2==3: #float
            raw = fData[3][fieldMeta.v2]
            val = formatfloat(raw)
            if val[-2:] == ".0": val = val[:-2]
            val = str2(val)
            val.raw = raw
        elif fieldMeta.type2==4: #keyword
            val = str2(fData[4][fieldMeta.v2])
        elif fieldMeta.type2==5: #vector3
            raw = fData[5][fieldMeta.v2]
            val = str2(", ".join([formatfloat(elem) for elem in raw]))
            val.raw=raw
        elif fieldMeta.type2==6: #vector4
            raw = fData[6][fieldMeta.v2]
            val = str2(", ".join([formatfloat(elem) for elem in raw]))
            val.raw=raw
        elif fieldMeta.type2==7: #string
            val = str2(fData[7][fieldMeta.v2])
        else: #ref
            val = createHierarchy(fData, fieldMeta.v2)

            
        if fieldMeta.type1 == 4:
            fields[fData[4][fieldMeta.v1]] = val
        else:
            fields[str(int(fData[3][fieldMeta.v1]))] = val
        
    fields.name = name
    return fields
  

###convert a namedDict to text without any special handling. 
##def genericPrintFields(fields, tabLevel = 0):
##    global f2
##    sep = "\t"
##    for name, field in fields.iteritems():
##        if type(field)==type(namedDict()):
##            if field.name:
##                raw_input("field.name detected")
##                asdf
##            f2.write(sep*tabLevel+name+"\n")
####            f2.write(sep*(tabLevel+1)+(field.name+" " or "")+name+"\n")
##            genericPrintFields(field, tabLevel+1)
##        else:
##            f2.write(sep*tabLevel + name + " "+str(field)+"\n")


def fileIo(f):
    global keywords, floats, assigns, vector3s, vector4s, strings
    fData = readFile(f)
    return createHierarchy(fData, 0)
    
##    genericPrintFields(fields)
