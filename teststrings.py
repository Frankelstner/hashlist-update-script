import os, sys, re
from struct import unpack, pack
from cStringIO import StringIO
from binascii import hexlify, unhexlify
import extract, converter
from ctypes import *
from hasher import hasher

p = r"C:\Program Files (x86)\Steam\steamapps\common\PAYDAY 2\assets"
p2 = r"D:\hexing\hexing pd2\^PD2\lua"



def isnum(n):
    try: float(n)
    except ValueError: return 0
    return 1

if not os.path.exists(p2):
    print "Invalid path p2 to Lua repo."
    print "It should point either directly at a folder (which contains 'core' and 'lib')"
    print "or point at a folder that contains folders where the names are version numbers,"
    print "e.g. 150, 164.1, and the script will automatically select the greatest number."
    asfd # Invalid path p2 to Lua repo.

try:
    p2 = p2 + "\\" + sorted([i for i in os.listdir(p2) if isnum(i)])[-1]
    print "Using",p2
except IndexError:
    # Assume that the path is given directly.
    assert "core" in os.listdir(p2) # Cannot find any Lua path.
    print "Using",p2
    


from time import time

def GetUnknownHashes(hashlist = None):
    """Return set of all path hashes that we cannot resolve yet."""
    if not hashlist: hashlist = extract.HashList()
    db = extract.ReadDb()
    unknowns = set()
    for (hash1, hash2) in db.values():
        if hash1 not in hashlist: unknowns.add(hash1)
        if hash2 not in hashlist: unknowns.add(hash2)
    for hash1 in GetBundleHashes():
        
        if hash1 not in hashlist:
            unknowns.add(hash1)
    
    return unknowns


def GetBundleHashes():
    vals = []
    for bundle in os.listdir(p):
        if len(bundle)==23 and bundle.endswith(".bundle"):
            val = int(bundle[14:16]+bundle[12:14]+bundle[10:12]+bundle[8:10]+bundle[6:8]+bundle[4:6]+bundle[2:4]+bundle[0:2],16)
            vals.append(val)
    return vals 



def GetAllHashes():
    """Return set of all hashes."""
    db = extract.ReadDb()
    hashes = set()
    for (hash1, hash2) in db.values():
        hashes.add(hash1)
        hashes.add(hash2)
    for hash1 in GetBundleHashes():
        hashes.add(hash1)
    return hashes


def GetUnknownLanguageHashes(hashlist = None):
    """Return set of all language hashes that we cannot resolve yet."""
    if not hashlist: hashlist = extract.HashList()

    unknowns=set()


    for data in extract.ExtractType("strings"):
        f = StringIO(data)

        version, count1, count2, metaSize, version2, n1, n2, smallN, minus1, null = unpack("10I",f.read(40))
        if not metaSize:
            f.close()
            continue
        assert metaSize == 0x28
        for entry in xrange(count1):
            hashed = unpack("Q",f.read(8))[0]
            if hashed not in hashlist:
                unknowns.add(hashed)
            f.seek(16,1)
##                const = f.read(4)
##                stringOffset = unpack("I",f.read(4))[0]
##                junk = f.read(4)
##                junk2 = f.read(4)
    return unknowns

def GetAllLanguageHashes():
    """Return set of all language hashes."""
    hashes=set()

    
    for data in extract.ExtractType("strings"):
        f = StringIO(data)
##            print path 
        version, count1, count2, metaSize, version2, n1, n2, smallN, minus1, null = unpack("10I",f.read(40))
        if not metaSize:
            f.close()
            continue
        assert metaSize == 0x28
        for entry in xrange(count1):
            hashes.add(unpack("Q",f.read(8))[0])
            f.seek(16,1)
    return hashes



import zlib

def Regex(unknowns):
    """Retrieve all strings from all files and check if their hash is wanted.

    Reduces unknown hashes from 8734 to 5989 (so about 30% less).
    Lowers unknown files from 11000 to 6000 (the same name/hash may appear several times with extensions)."""

    myre = re.compile(r"""[A-Za-z0-9%\-_/\\ ]{7,}""")
    
    found = set()


    # Check all files in the bundles.
    

    for data in extract.ExtractType():
        if data and data[0]=="\x78":
            try:
                data = zlib.decompress(data)
            except zlib.error:
                pass
            
        for hit in myre.finditer(data):
            test = hit.group()
            if hasher(test) in unknowns:
                found.add(test)
            lowered = test.lower()
            if hasher(lowered) in unknowns:
                found.add(lowered)

            if lowered.startswith("play_"):
                play = test[5:]
                if hasher(play) in unknowns:
                    found.add(play)
                    print "##############################################"
                play+="_"
                
                for i in xrange(100):
                    play2 = play+("0"+str(i) if i<10 else str(i))
                    if hasher(play2) in unknowns:
                        found.add(play2)
                        print "##############################################"

                    
            else:

                for suffix in ["_thq","_cc","_cc_thq","_contour"]:
                    if hasher(test+suffix) in unknowns:
                        found.add(test+suffix)
                    if hasher(lowered+suffix) in unknowns:
                        found.add(lowered+suffix)
                        

    myre = re.compile(r"""[A-Za-z0-9%\-_/\\ ]{6,}""")


    # Use Lua dump.
    for dir0, dirs, ff in os.walk(p2):
        for fname in ff:
            data = open(dir0+"/"+fname,"rb").read()
            for hit in myre.finditer(data):
                test = hit.group()
                if hasher(test) in unknowns:
                    found.add(test)
                lowered = test.lower()
                if hasher(lowered) in unknowns:
                    found.add(lowered)

                for suffix in ["_thq","_cc","_cc_thq","_contour","_desc","_longdesc", "_income", "_progress"]:
                    if hasher(test+suffix) in unknowns:
                        found.add(test+suffix)  
                    if hasher(lowered+suffix) in unknowns:
                        found.add(lowered+suffix)


                if not "_" in test: continue
                
                test = "_".join(test.split("_")[:-1])
                if hasher(test) in unknowns:
                    found.add(test)
                lowered = test.lower()
                if hasher(lowered) in unknowns:
                    found.add(lowered)

                for suffix in ["_thq","_cc","_cc_thq","_contour","_desc","_longdesc", "_income", "_progress"]:
                    if hasher(test+suffix) in unknowns:
                        found.add(test+suffix)  
                    if hasher(lowered+suffix) in unknowns:
                        found.add(lowered+suffix)
                
##     # Also run over the bundle Lua.
##    for data in extract.ExtractType("lua"):
##        for hit in myre.finditer(data):
##            test = hit.group()
##            if hasher(test) in unknowns:
##                found.add(test)
##            lowered = test.lower()
##            if hasher(lowered) in unknowns:
##                found.add(lowered)
##
##            for suffix in ["_thq","_cc","_cc_thq","_contour","_desc","_longdesc", "_income", "_progress"]:
##                if hasher(test+suffix) in unknowns:
##                    found.add(test+suffix)  
##                if hasher(lowered+suffix) in unknowns:
##                    found.add(lowered+suffix)
##
##
##            if not "_" in test: continue
##            
##            test = "_".join(test.split("_")[:-1])
##            if hasher(test) in unknowns:
##                found.add(test)
##            lowered = test.lower()
##            if hasher(lowered) in unknowns:
##                found.add(lowered)
##
##            for suffix in ["_thq","_cc","_cc_thq","_contour","_desc","_longdesc", "_income", "_progress"]:
##                if hasher(test+suffix) in unknowns:
##                    found.add(test+suffix)  
##                if hasher(lowered+suffix) in unknowns:
##                    found.add(lowered+suffix)



    
    return found


def ReadNullTerminatedString(f):
    s = ""
    while 1:
        c = f.read(1)
        if c=="\0": break
        s+=c
    return s
def Streams(unknowns):
    """Resolve all .stream files."""
    found=set()
    for bnk in extract.ExtractType("banksinfo"):
        f=StringIO(bnk)
        count1, count12, size1, n, \
        n1, count2, count22, n2, \
        nn, n12, null, count3, \
        count32, size, something, somesize = unpack("16I",f.read(64))

        assert count2==count22
        
        f.seek(size1 + 8*count1)
        strings = [ReadNullTerminatedString(f) for i in xrange(count1)]

        f.seek(count2*16, 1)
        f.seek(count22*16, 1)

        nums = [ReadNullTerminatedString(f) for i in xrange(count2)]

        strings = ["soundbanks/streamed/"+s[11:]+"/" for s in strings]

        for s in strings:
            for n in nums:
                if hasher(s+n) in unknowns:
                    found.add(s+n)
    return found



def Dialog(unknowns):
    """Resolve all .dialog files."""
    found = set()
    for entry in converter.fileIo(StringIO(extract.ExtractPath("gamedata/dialogs/index"))).values():
        test = "gamedata/dialogs/"+entry["name"]
        if hasher(test) in unknowns: found.add(test)
    return found



# Go along the hierarchy of mission files.
# Start at levelstweakdata.

def Missions(unknowns):
    """Resolve all files related to missions."""
    found = set()
    
    def ProcessLevel(dirName):
        """Given a source path pointing to a mission or instance folder, process everything inside."""

        # Try some common names first.
        for fname in ["world","world_cameras","world_sounds","nav_manager_data","mission","massunit","cover_data",
                      "continents", "continent", "editor_only/editor_only","world/world", "cube_lights/dome_occlusion"]:
            test = dirName+fname
            if hasher(test) in unknowns: found.add(test)

        # Go into the world.world file.
        try:
            world = extract.ExtractPath(dirName + "world")
        except KeyError:
            return
##        print dirName
        
        world = converter.fileIo(StringIO(world))

        # The file defines some other files, but actually the common guesses take care of them. So ignore this part.
        for key, val in world.iteritems():
            fname = val.get("file",0) or val.get("continents_file", 0)
            if not fname: continue
            test = dirName + fname
            if hasher(test) in unknowns: found.add(test)
        ###


        # World file done. Now grab the folders given in the mission file. 

        continentPaths = [dirName+val.values()[0] for val in converter.fileIo(StringIO(extract.ExtractPath(dirName+"mission"))).values()]

        for test in continentPaths:
            if hasher(test) in unknowns:
                found.add(test)
        

        # The continents define statics and instances.
        # There is a continent in each subfolder.
        #   static -> unit_data -> unit_id.

        lights = set()
        instances = set()
        for continent in extract.ExtractPathTypes(continentPaths,"continent"):
            struct = converter.fileIo(StringIO(continent))
            if "statics" in struct:
                for index, val in struct["statics"].iteritems():
                    unit_data = val["unit_data"]
                    if "lights" in unit_data:
                        test = dirName + "cube_lights/" + unit_data["unit_id"]
                        if hasher(test) in unknowns: found.add(test)

     
            # Recurse into instances.
            if "instances" in struct:
                for index, val in struct["instances"].iteritems():
                    ProcessLevel(os.path.dirname(val["folder"])+"/")
      

    # Get level name from Lua and the most common file names.
    for line in open(p2+"/lib/tweak_data/levelstweakdata.lua","rb"):
        if "world_name" and '"' in line:
            path = "".join(line.split('"')[1])
            if not path: continue
            ProcessLevel("levels/"+path+"/")

    return found

    




def Icons(unknowns):
    """GUI icons."""

    found = set()

    # trophy_image:set_image("guis/dlcs/chill/textures/pd2/trophies/" .. tostring(data.image_id))
    
    for line in open(p2+"/lib/tweak_data/customsafehousetweakdata.lua","rb"):
        if "image_id = " in line:
            rhs = line.strip().split('"')[1]
            test = "guis/dlcs/chill/textures/pd2/trophies/"+rhs
            if hasher(test) in unknowns:
                found.add(test)


    # Weapons
    """new_data.bitmap_texture = guis_catalog .. "textures/pd2/blackmarket/icons/melee_weapons/" .. tostring(new_data.name)
    table.insert(new_data.extra_bitmaps, guis_catalog .. "textures/pd2/blackmarket/icons/weapons/" .. tostring(primary_id))
    table.insert(new_data.extra_bitmaps, guis_catalog .. "textures/pd2/blackmarket/icons/weapons/" .. tostring(secondary_id))"""

    for line in open(p2+"/lib/tweak_data/weapontweakdata.lua","rb"):
        if "texture_bundle_folder = " in line:
            lhs, rhs = line.split("=")
            name = lhs.split(".")[1]
            dlc = rhs.split('"')[1]
            test = "guis/dlcs/"+dlc+"/textures/pd2/blackmarket/icons/weapons/"+name
            if hasher(test) in unknowns:
                found.add(test)

    # Weapon mods
    """local guis_catalog = "guis/"
    local bundle_folder = part.texture_bundle_folder
    if bundle_folder then
            guis_catalog = guis_catalog .. "dlcs/" .. tostring(bundle_folder) .. "/"
    end
    guis_catalog = guis_catalog .. "textures/pd2/blackmarket/icons/mods/"
    if not DB:has(Idstring("texture"), guis_catalog .. id) then
            print(guis_catalog .. id)"""

    curWeap=None
    for line in open(p2+"/lib/tweak_data/weaponfactorytweakdata.lua","rb"):
        if "\tself.parts" in line:
            try:
                curWeap = (line.split(".")[2]).split(" ")[0]
            except IndexError:
                if "\tself.parts = {}" not in line:
                    print "possible error",line
                continue

        if "\ttexture_bundle_folder" in line:
            dlc = line.split('"')[1]
            test = "guis/dlcs/"+dlc+"/textures/pd2/blackmarket/icons/mods/"+curWeap
            if hasher(test) in unknowns:
                found.add(test)
            

    # Melee
    """guis\dlcs\west\textures\pd2\blackmarket\icons\melee_weapons"""
    name=dlc=None
    for line in open(p2+"/lib/tweak_data/blackmarket/meleeweaponstweakdata.lua","rb"):
        if "self.melee_weapons." in line:
            name = line.split(".")[2]
        if "texture_bundle_folder = " in line and '"' in line:
            dlc = line.split('"')[1]

        if name and dlc:
            test = "guis/dlcs/"+dlc+"/textures/pd2/blackmarket/icons/melee_weapons/"+name
            if hasher(test) in unknowns:
                found.add(test)



    # Skins
    def GetWeaponNames():
        data = open(p2+"/lib/tweak_data/weapontweakdata.lua","rb").read()
        regex = re.compile(r"self\.(\w*)")
        names = set()
        for match in regex.finditer(data):
            names.add( match.groups(1)[0])
        return names

    dlcs = set()
    name=dlc=None
    for line in open(p2+"/lib/tweak_data/blackmarket/weaponskinstweakdata.lua","rb"):
        if "self.weapon_skins." in line:
            name = line.split(".")[2]
            if "=" in name:
                name = name.replace("{","").replace("=","").strip()

        if "texture_bundle_folder = " in line and '"' in line:
            dlc = line.split('"')[1]

        if name and dlc:
##        if "texture_bundle_folder = " in line:
##            lhs, rhs = line.split("=")
##            name = lhs.split(".")[2]
##            dlc = rhs.split('"')[1]
            test = "guis/dlcs/"+dlc+"/weapon_skins/"+name
            dlcs.add((dlc, "_"+dlc.split("/")[-1]))
##
##            """guis\dlcs\cash\safes\sputnik\weapon_skins\p90_woodland"""

            test = "guis/dlcs/"+dlc+"/textures/pd2/blackmarket/icons/melee_weapons/"+name
##            print test
            if hasher(test) in unknowns:
##                hjj
                found.add(test)


    for weapName in GetWeaponNames():
        for dlcPath, dlc in dlcs:
            """guis/dlcs/cash/safes/smosh/weapon_skins/wa2000_smosh"""
            test = "guis/dlcs/"+dlcPath+"/weapon_skins/"+weapName+dlc
##            print test
            if hasher(test) in unknowns:
                found.add(test)
##                gfhj
            
                
                

        



    # Masks

    for line in open(p2+"/lib/tweak_data/blackmarket/maskstweakdata.lua","rb"):
        if "texture_bundle_folder = " in line or ".dlc = " in line:
            lhs, rhs = line.split("=")
            try:
                name = lhs.split(".")[2]
                dlc = rhs.split('"')[1]
                test = "guis/dlcs/"+dlc+"/textures/pd2/blackmarket/icons/masks/"+name
                if hasher(test) in unknowns:
                    found.add(test)
                test = "guis/textures/pd2/blackmarket/icons/masks/"+name
                if hasher(test) in unknowns:
                    found.add(test)      
            except IndexError: continue  
    

    # Materials
    """ guis\dlcs\bbq\textures\pd2\blackmarket\icons\materials
        guis\textures\pd2\blackmarket\icons\materials"""
    
    for line in open(p2+"/lib/tweak_data/blackmarket/materialstweakdata.lua","rb"):
        if "texture_bundle_folder = " in line or ".dlc = " in line:
            lhs, rhs = line.split("=")
            try:
                name = lhs.split(".")[2]
                dlc = rhs.split('"')[1]
                test = "guis/dlcs/"+dlc+"/textures/pd2/blackmarket/icons/materials/"+name
                if hasher(test) in unknowns:
                    found.add(test)
                test = "guis/textures/pd2/blackmarket/icons/materials/"+name
                if hasher(test) in unknowns:
                    found.add(test)

                    
            except IndexError: continue


    # Grenades
    """guis\dlcs\west\textures\pd2\blackmarket\icons\grenades"""

    name=dlc=None
    for line in open(p2+"/lib/tweak_data/blackmarket/projectilestweakdata.lua","rb"):
        if "self.projectiles." in line:
            name = line.split(".")[2]
            if "=" in name:
                name = name.replace("{","").replace("=","").strip()

        if "texture_bundle_folder = " in line and '"' in line:
            dlc = line.split('"')[1]

        if name and dlc:
            test = "guis/dlcs/"+dlc+"/textures/pd2/blackmarket/icons/grenades/"+name
            if hasher(test) in unknowns:
                found.add(test)


    # Safes, drills
    """guis\dlcs\cash\safes\bah\safes\event_bah"""
    
    name=dlc=None
    for line in open(p2+"/lib/tweak_data/economytweakdata.lua","rb"):
        if "texture_bundle_folder = " in line and '"' in line:
            dlc = line.split('"')[1]
            
        if "self.safes." in line:
            name = line.split(".")[2]
            if "=" in name:
                name = name.replace("{","").replace("=","").strip()
            
        
        if name and dlc:
            test = "guis/dlcs/"+dlc+"/safes/"+name
            if hasher(test) in unknowns:
                found.add(test)
##                ghk
            test = "guis/dlcs/"+dlc+"/drills/"+name
            if hasher(test) in unknowns:
                found.add(test)
##                ghjk




    # Safehouse character portraits
    """self.map.rooms.german = {
            x = 1237,
            y = 1108,
            icon = "safehouse_character_icon_wolf"
    }"""


    """texture = "guis/dlcs/chill/textures/pd2/character_icon/" .. tweak_table.icon"""

    for line in open(p2+"/lib/tweak_data/customsafehousetweakdata.lua","rb"):
        if "icon = " in line:
            name = line.split('"')[1]   # "safehouse_character_icon_jimmy"
            test = "guis/dlcs/chill/textures/pd2/character_icon/"+name
            if hasher(test) in unknowns:
                found.add(test)



    # Tips
    """guis/textures/loading/hints/" .. tip.image)"""
    for line in open(p2+"/lib/tweak_data/tipstweakdata.lua","rb"):
        if 'image = "' in line:
            lhs, rhs = line.split("=")
            rhs = rhs.split('"')[1]   # e.g. "contact_locke"
            test = "guis/textures/loading/hints/"+rhs
            if hasher(test) in unknowns:
                found.add(test)

    return found




def Skins(unknowns):
    """Go through the Lua to grab all idstrings, then play around with the ints at the end."""
    found=set()


    words = [line[:-2].lower() for line in open("dict.txt","rb")]
    words.extend([str(i) for i in xrange(100)])
    for i in xrange(100):
        if i<10:
            words.append("0"+str(i))
        else:
            words.append(str(i))
    for i in xrange(100):
        if i<10:
            words.append("00"+str(i))
        else:
            words.append("0"+str(i))
    for c in "abcdefghijklmnopqrstuvwxyz":
        words.append(c)
    for c in "abcdefghijklmnopqrstuvwxyz":
        for c2 in "abcdefghijklmnopqrstuvwxyz":
            words.append(c+c2)
    
    for line in open(p2+"/lib/tweak_data/blackmarket/weaponskinstweakdata.lua","rb"):
        if "Idstring(" in line:
            line = (line.split("Idstring(")[1]).split('"')[1]
            name = line.split("/")[-1]

            for i in xrange(len(line)-len(name),len(line)-3):
                try:
                    int(line[i:i+3])

                    leftPath = line[:i]
                    rightPart = line[i+3:]

                    for sw in [0,1]:
                        for i in xrange(50):
                            ic = ("00"+str(i) if i<10 else "0"+str(i)) if sw else str(i)
                            test = leftPath+ic+rightPart
                            if hasher(test) in unknowns:
                                found.add(test)
                            
                            for c in "abcdefghijklmnopqrstuvwxyz":
                                test = leftPath+ic+c+rightPart
                                if hasher(test) in unknowns:
                                    found.add(test)

                                test = leftPath+ic+"_"+c+rightPart
                                if hasher(test) in unknowns:
                                    found.add(test)

                                test = leftPath+c+ic+rightPart
                                if hasher(test) in unknowns:
                                    found.add(test)

                                test = leftPath+c+"_"+ic+rightPart
                                if hasher(test) in unknowns:
                                    found.add(test)

                    for c in "abcdefghijklmnopqrstuvwxyz":
                        test = leftPath+c+rightPart
                        if hasher(test) in unknowns:
                            found.add(test)

                    
                except ValueError: continue

    return found




def Preload(unknowns):
    """Resolve .gui files. New contacts will require manual fiddling, because I do not understand where they come from."""

    suffixes = ["weapon_icons", "mask_icons", "contact_locke",
                "contact_jimmy", "contact_the_butcher", "contact_the_dentist",
                "preplanning", "blackmarket", "game_base", "start_menu",]


    found = set()
    regex = re.compile("guis/dlcs/[^/]+")
    for data in extract.ExtractType("gui"):
        for match in regex.finditer(data):
            for suffix in suffixes:
                test = match.group()+"/guis/preload_"+suffix
                if hasher(test) in unknowns:
                    found.add(test)
    
    return found


def MatConfigs(unknowns):
    """Use the existing hashlist to guess material configs."""
    hl = extract.HashList()
    found = set()
    for suffix in ["_cc","_thq","_cc_thq", "_contour", "_husk"]:
        for path in hl.values():
            test = path+suffix
            if hasher(test) in unknowns:
                found.add(test)
                
            test = "_".join(path.split("_")[:-2])+suffix
            if hasher(test) in unknowns:
                found.add(test)

    return found



def LuaNames(unused):
    """Resolve all .Lua files. This uses the names from the Lua dump."""
    found = set()
    hashlist = extract.HashList()

    for dir0, dirs, ff in os.walk(p2):
        for fname in ff:
            if not fname.endswith(".lua"): continue
            path = (dir0[len(p2)+1:]+"/"+fname[:-4]).lower().replace("\\","/")

            if hasher(path) not in hashlist:
                found.add(path)
    return found



def Language(unknowns):
    found = set()
    for line in open(p2+"/lib/tweak_data/customsafehousetweakdata.lua","rb"):
        if "help_id = " in line:
            rhs = line.strip().split('"')[1]
            for i in xrange(10):
                test = rhs+"_"+str(i)
                if hasher(test) in unknowns:
                    found.add(test)

        if line.startswith("\tself.heisters."):
            charname = line.split(".")[2]
            test = "hud_int_talk_"+charname
            if hasher(test) in unknowns:
                found.add(test)

    for i in xrange(1000):
        test = "menu_jukebox_track_"+str(i)
        if hasher(test) in unknowns:
            found.add(test)
    

    for line in open(p2+"/lib/tweak_data/levelstweakdata.lua","rb"):
        if ".world_name =" in line:
            name = line.split(".")[1]
            heist = "heist_"+name
            if hasher(heist) in unknowns:
                found.add(heist)

            test = heist+"_crimenet"
            if hasher(test) in unknowns:
                found.add(test)
            test = heist+"_briefing"
            if hasher(test) in unknowns:
                found.add(test)



    
    for line in open(p2+"/lib/tweak_data/levelstweakdata.lua","rb"):
        if ".briefing_dialog = " in line:
            try:
                rhs = line.split('"')[1]
            except IndexError: continue
            if rhs[:5] in ("Play_", "play_"):
                rhs=rhs[5:]
            if hasher(rhs) in unknowns:
                found.add(rhs)


    for line in open(p2+"/lib/tweak_data/levelstweakdata.lua","rb"):
        if ".outro_event =" in line:
            try:
                rhs = line.strip().split('"')[1]
            except IndexError: continue
            if rhs[:5] in ("Play_", "play_"):
                rhs=rhs[5:]
            parts = rhs.split("_")
            for i in xrange(len(parts)-1,-1,-1):
                try:
                    int(parts[i])
                except ValueError:
                    break

            parts = parts[:i+1]
            if len(parts)<2: continue
            lastPart = ""
            for c in parts[-1]:
                try:
                    int(c)
                    break
                except ValueError:
                    lastPart+=c

            parts[-1]=lastPart

            rhs = "_".join(parts)

            for i in xrange(51):
                i2 = rhs+("0"+str(i) if i<10 else str(i))+"_"
                for j in xrange(51):
                    j2 = i2+("0"+str(j) if j<10 else str(j))+"_"
                    for k in xrange(51):
                        test = j2+str("0"+str(k) if k<10 else str(k))
                        if hasher(test) in unknowns:
                            found.add(test)


        if ".intro_event = " in line:
            try:
                rhs = line.strip().split('"')[1]
            except IndexError: continue
            if rhs[:5] in ("Play_", "play_"):
                rhs=rhs[5:]
            print rhs
            if "_intro" in rhs:
                rhs = rhs[:rhs.index("_intro")]
            rhs+="_"

            for i in xrange(51):
                i2 = rhs+("0"+str(i) if i<10 else str(i))+"_"
                for j in xrange(51):
                    test = i2+str("0"+str(j) if j<10 else str(j))
                    if hasher(test) in unknowns:
                        found.add(test)


    for line in open(p2+"/lib/tweak_data/economytweakdata.lua","rb"):
        if line.startswith("\tself.drills.") and "safe = " in line:
            rhs = line.split('"')[1]
            test = "menu_steam_market_content_"+rhs
            if hasher(test) in unknowns:
                found.add(test)
            test = "steam_inventory_collection_"+rhs
            if hasher(test) in unknowns:
                found.add(test)

    return found


def Movies(unknowns):
    found = set()
    for line in open(p2+"/lib/tweak_data/narrativetweakdata.lua","rb"):
        if '"' in line:
            test = "movies/"+line.split('"')[1]
            if hasher(test) in unknowns:
                found.add(test)

    for line in open(p2+"/lib/tweak_data/guitweakdata.lua","rb"):
        if '"' in line:
            test = "movies/codex/"+line.split('"')[1]
            if hasher(test) in unknowns:
                found.add(test)
    return found








# The idea is:
#   To find hashes, use the huge hashlist because:
#       1) MatConfigs depends on it.
#       2) The game might add and remove different files with different updates.
#
#   When the newest hashes are found, add them to the huge hashlist.
#   For actual extraction, remove all unused entries from the huge hashlist and publish the resulting stripped-down hashlist.




##lls = """guis/dlcs/spa/textures/pd2/blackmarket/icons/weapons/tti
##guis/dlcs/spa/textures/pd2/blackmarket/icons/melee_weapons/catch
##guis/dlcs/spa/textures/pd2/blackmarket/icons/masks/spa_01
##guis/dlcs/spa/textures/pd2/blackmarket/icons/masks/spa_02
##guis/dlcs/spa/textures/pd2/blackmarket/icons/masks/spa_03
##guis/dlcs/spa/textures/pd2/blackmarket/icons/masks/spa_04"""
##
##lines = lls.split("\n")
##for line in lines:
##    print hasher(line) in un

def Custom(unknowns):
    found = set()
    for s in ["dlr_flt_","chz_flt_", "thg_flt_","bnc_flt_","fri_brf_", "pln_fri_"]:
        for i in xrange(100):
            i2 = s+("0"+str(i) if i<10 else str(i))+"_"
            for j in xrange(100):
                test = i2+("0"+str(j) if j<10 else str(j))
                if hasher(test) in un:
                    found.add(test)
                    
                    
    for i in xrange(51):
        i2 = "pln_fri_"+("0"+str(i) if i<10 else str(i))+"_"
        for j in xrange(51):
            j2 = i2+("0"+str(j) if j<10 else str(j))+"_"
            for k in xrange(51):
                test = j2+str("0"+str(k) if k<10 else str(k))
                if hasher(test) in unknowns:
                    found.add(test)
                       
    return found


def Packages(unknowns):
    found = set()
    prefix = "packages/dlcs/"
    suffix = "/start_menu"
    for line in open(p2+"/lib/tweak_data/dlctweakdata.lua","rb"):
        line=line.strip().split("=")
        if len(line)>1:
            test = prefix+line[0].strip()+suffix
##            print test
            if hasher(test) in unknowns:
                found.add(test)
        

    for line in open(p2+"/lib/tweak_data/narrativetweakdata.lua","rb"):
        if "packages" in line:
            line=line.strip().split('"')
            if len(line)>1:
                test = line[1]
                if hasher(test) in unknowns:
                    found.add(test)

    for test in ("normal","hard","overkill","overkill_145","easy_wish","overkill_290","sm_wish"):
        test = "packages/"+test
        if hasher(test) in unknowns:
            found.add(test)
            
    for line in open(p2+"/lib/tweak_data/levelstweakdata.lua","rb"):
        if "packages" in line:
            line = line.split('"')
            if len(line)>1:
                test = line[1]
                if hasher(test) in unknowns:
                    found.add(test)
     
    return found



def PackagePost(unknowns):
    """Derive more strings from the hashlist itself."""
    found = set()
    for item in hashlist.itervalues():
        test = item+"_init"
        if hasher(test) in unknowns:
            found.add(test)
        test = item+"_sounds"
        if hasher(test) in unknowns:
            found.add(test)
            
    return found



##hashlist = extract.HashList()
##unknowns = set()
##for hash1 in GetBundleHashes():
##    if hash1 not in hashlist:
##        unknowns.add(hash1)
##
##
##for item in hashlist.itervalues():
##    if hasher(item+"_init") in unknowns:
##        asdf
##
##prefix = "packages/fps_weapon_parts/"
##for item in hashlist.itervalues():
##    for part in item.split("/"):
##        if hasher(prefix+part) in unknowns:
##            asdf

##for bundle in os.listdir(p):
##    if len(bundle)==23 and bundle.endswith(".bundle"):
##        val = int(bundle[14:16]+bundle[12:14]+bundle[10:12]+bundle[8:10]+bundle[6:8]+bundle[4:6]+bundle[2:4]+bundle[0:2],16)
##        if val in unknowns:
##            print bundle


##print len(Packages(unknowns)), len(unknowns)
##print len(PackagePost(unknowns)), len(unknowns)
##ggg




##asdf

##entries = {}
####hashlist = extract.HashList()
##
##for line in open("text.txt","rb"):
##    parts = line.split('"')
##    entries[parts[1]]=parts[3]
##
##c1,c2 =0,0
##
##for entry in entries:
##    if hasher(entry) in un:
##        c1+=1
##    else:
##        c2+=1
####        print entry
##
##
##asdf


un = GetUnknownHashes()
un|= GetUnknownLanguageHashes()


hashlist = extract.HashList()
found = set()
for method in [Custom]:
    found|=method(un)
    un-=found
    hashlist.update({hasher(i):i for i in found})


for method in [Regex, Streams, Dialog, Missions, Icons, Skins, Preload, MatConfigs, LuaNames, Language, Movies, Packages, PackagePost]:
##for method in [Dialog, Missions, Icons, Skins, Preload, MatConfigs, LuaNames, Language, Movies, Packages, PackagePost]:
##for method in [PackagePost]:
##for method in [Icons, Skins, Preload, MatConfigs, LuaNames, Language, Movies, Packages, PackagePost]:
##for method in [Missions]:
    print method
    found|=method(un)
    un-=found
    hashlist.update({hasher(i):i for i in found})

##
#### Write new entries to hashlistFULL.

f2 = open("hashlistFULL","a+b")
for path in found:
    f2.write(path+"\r\n")
f2.close()



def CleanHashList():
    """Remove anything that is not used."""
    f2 = open("hashlist","wb")
    hashes = GetAllHashes()
    hashes|= GetAllLanguageHashes()
    
    for line in open("hashlistFULL","rb"):
        if line.endswith("\r\n"):line=line[:-2]
        elif line.endswith("\r"):line=line[:-1]
        elif line.endswith("\n"):line=line[:-1]
        
        if line and hasher(line) in hashes:
            f2.write(line+"\r\n")
    f2.close()



CleanHashList()










































